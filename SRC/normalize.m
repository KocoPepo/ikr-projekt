function X = normalize(X)

mean_val = 0;
% normalize each MFCC coefficient (13 MFCC coef.)
for ii = 1:13
	mean_val = mean(X(ii,:));
	X(ii,:) = X(ii,:) - mean_val;
end