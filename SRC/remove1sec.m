% vymaze prvych 100 ramcov
function X = remove1sec(X)

n_frames = 100; % first 100 frames to delete
X(:,1:n_frames) = [];

