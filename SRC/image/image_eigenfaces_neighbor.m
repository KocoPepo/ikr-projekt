% IKR projekt 2015 - Lucansky, Cervicek, Kocour
addpath('lib');

%train_t = cell2mat(png2fea('target_train'));
%dev_t = cell2mat(png2fea('target_dev'));
%train_n = cell2mat(png2fea('non-target_train'));
%dev_n = cell2mat(png2fea('non-target_dev'));

% Cells
images = {};
T = [];

for class = 1:31
  train_images = png2fea(['train/',num2str(class)]);
  num_of_images = size(train_images,2);
  images = [images train_images];
    
  T = [T repmat(class, 1, num_of_images)];
  
%%%%%%%
  train_images = png2fea(['dev/',num2str(class)]);
  num_of_images = size(train_images,2);
  images = [images train_images];
    
  T = [T repmat(class, 1, num_of_images)];

end
images = cell2mat(images);

% DoG (Difference of Gaussians) - band pass filter
for x = 1:size(images, 2)
    images(:,x) = histeq(uint8(dog(images(:,x),0.7,1.5,1)));
end

%%
%images = [train_t dev_t train_n dev_n];
%T = [ones(1, size(train_t, 2)) ones(1, size(dev_t, 2)) zeros(1, size(train_n, 2)) zeros(1, size(dev_n, 2))];

eval_t = cell2mat(png2fea('eval'));
% DoG (Difference of Gaussians) - band pass filter
for x = 1:size(eval_t, 2)
    eval_t(:,x) = histeq(uint8(dog(eval_t(:,x),0.7,1.5,1)));
end

mean_face = mean(images, 2);
shifted_images = images - repmat(mean_face, [1, size(images,2)]);

[evectors, score, evalues] = princomp(images');

%num_eigenfaces = 20;
%evectors = evectors(:, 1:num_eigenfaces);
%feature_vec = evectors' * (input_image(:) - mean_face);

features = evectors' * shifted_images;
%%
scores = ones(1, size(eval_t, 2));
for x = 1:size(eval_t, 2)
    input_image = eval_t(:,x);
    % calculate the similarity of the input to each training image
    feature_vec = evectors' * (input_image - mean_face);
    similarity_score = arrayfun(@(n) 1 / (1 + norm(features(:,n) - feature_vec)), 1:size(images,2));

    % find the image with the highest similarity
    [match_score, match_ix] = max(similarity_score);
    scores(x) = T(match_ix);
end


%%
result_file = fopen('image_eigenfaces_neighbor','w');

for x = 1:size(eval_t, 2)
   fprintf(result_file, 'eval_%05d %d', x, scores(:,x));
   for z = 1:31
      fprintf(result_file, ' Nan');
   end
   fprintf(result_file, '\n');
end
fclose(result_file);
