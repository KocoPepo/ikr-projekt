function [Ws_t, MUs_t, COVs_t, TTL_t]=train_audio(dirname, num)
% funkcia automaticky trenuje klasifikator
% dirname je nazov priecinku s wav nahravkami
% num je pocet gausoviek
% vracia GMM model

% nahrame si trenovacie data so suboru
train = wav16khz2mfcc(dirname);

% Vymazat prvu sekundu, koli vybijaniu kondenzatora na microfone!!!
% Vynormalizuj data, odstranim ticho
for ii = 1:length(train)
	train{ii} = remove1sec(train{ii});
	train{ii} = remove_silence(train{ii});
	train{ii} = normalize(train{ii});
end

% Spojime ukazatale na matice MFCC koeficientov do jednej velkej matice,
% kde budu jednotlive ramce za sebou
train=cell2mat(train);

% Nastavime parametre pre train_gmm na target
M_t = num;
MUs_t = train(:, random('unid', size(train, 2), 1, M_t));
COVs_t = repmat(var(train', 1)', 1, M_t);
Ws_t = ones(1,M_t) / M_t;

% Trenujem Klasifikator pomocou EM-algorytmu
%disp([dirname ': '])
for jj=1:30
  [Ws_t, MUs_t, COVs_t, TTL_t] = train_gmm(train, Ws_t, MUs_t, COVs_t); 
  %disp([' Iteration: ' num2str(jj) ' Total log-likelihood: ' num2str(TTL_t) ' for target; '])
end


