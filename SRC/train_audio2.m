function [Ws_t, MUs_t, COVs_t, TTL_t]=train_audio2(dirname, Ws_t, MUs_t, COVs_t, TTL_t)
% to iste co funkcia train_audio, len pretrenovava model
% dirname je nazov priecinku s wav nahravkami
% vracia pretrenovany GMM model

% nahrame si trenovacie data so suboru
train = wav16khz2mfcc(dirname);

% Vymazat prvu sekundu, koli vybijaniu kondenzatora na microfone!!!
% Vynormalizuj data, odstranim ticho
for ii = 1:length(train)
	train{ii} = remove1sec(train{ii});
	train{ii} = remove_silence(train{ii});
	train{ii} = normalize(train{ii});
end

% Spojime ukazatale na matice MFCC koeficientov do jednej velkej matice,
% kde budu jednotlive ramce za sebou
train=cell2mat(train);

% Trenujem Klasifikator pomocou EM-algorytmu
%disp([dirname ': '])
for jj=1:30
  [Ws_t, MUs_t, COVs_t, TTL_t] = train_gmm(train, Ws_t, MUs_t, COVs_t); 
  %disp([' Iteration: ' num2str(jj) ' Total log-likelihood: ' num2str(TTL_t) ' for target; '])
end


