% Martin Kocour (xkocou08) 
% IKR projekt - audio klasifikator
% Trenovanie audio klasifikatoru pomocou GMM modelu
% Target = Pavel Smrz
% Nontarget = Others


clear;
format long;
% cesta k projektu
cd ../

addpath('lib');
addpath('SRC');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%									%
%	Trenovanie 31 klasifikatorov	%
%									%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Ws_1, MUs_1, COVs_1, TLL_1] = train_audio('./train/1', 20);
[Ws_2, MUs_2, COVs_2, TLL_2] = train_audio('./train/2', 20);
[Ws_3, MUs_3, COVs_3, TLL_3] = train_audio('./train/3', 20);
[Ws_4, MUs_4, COVs_4, TLL_4] = train_audio('./train/4', 20);
[Ws_5, MUs_5, COVs_5, TLL_5] = train_audio('./train/5', 20);
[Ws_6, MUs_6, COVs_6, TLL_6] = train_audio('./train/6', 20);
[Ws_7, MUs_7, COVs_7, TLL_7] = train_audio('./train/7', 20);
[Ws_8, MUs_8, COVs_8, TLL_8] = train_audio('./train/8', 20);
[Ws_9, MUs_9, COVs_9, TLL_9] = train_audio('./train/9', 20);
[Ws_10, MUs_10, COVs_10, TLL_10] = train_audio('./train/10', 20);
[Ws_11, MUs_11, COVs_11, TLL_11] = train_audio('./train/11', 20);
[Ws_12, MUs_12, COVs_12, TLL_12] = train_audio('./train/12', 20);
[Ws_13, MUs_13, COVs_13, TLL_13] = train_audio('./train/13', 20);
[Ws_14, MUs_14, COVs_14, TLL_14] = train_audio('./train/14', 20);
[Ws_15, MUs_15, COVs_15, TLL_15] = train_audio('./train/15', 20);
[Ws_16, MUs_16, COVs_16, TLL_16] = train_audio('./train/16', 20);
[Ws_17, MUs_17, COVs_17, TLL_17] = train_audio('./train/17', 20);
[Ws_18, MUs_18, COVs_18, TLL_18] = train_audio('./train/18', 20);
[Ws_19, MUs_19, COVs_19, TLL_19] = train_audio('./train/19', 20);
[Ws_20, MUs_20, COVs_20, TLL_20] = train_audio('./train/20', 20);
[Ws_21, MUs_21, COVs_21, TLL_21] = train_audio('./train/21', 20);
[Ws_22, MUs_22, COVs_22, TLL_22] = train_audio('./train/22', 20);
[Ws_23, MUs_23, COVs_23, TLL_23] = train_audio('./train/23', 20);
[Ws_24, MUs_24, COVs_24, TLL_24] = train_audio('./train/24', 20);
[Ws_25, MUs_25, COVs_25, TLL_25] = train_audio('./train/25', 20);
[Ws_26, MUs_26, COVs_26, TLL_26] = train_audio('./train/26', 20);
[Ws_27, MUs_27, COVs_27, TLL_27] = train_audio('./train/27', 20);
[Ws_28, MUs_28, COVs_28, TLL_28] = train_audio('./train/28', 20);
[Ws_29, MUs_29, COVs_29, TLL_29] = train_audio('./train/29', 20);
[Ws_30, MUs_30, COVs_30, TLL_30] = train_audio('./train/30', 20);
[Ws_31, MUs_31, COVs_31, TLL_31] = train_audio('./train/31', 20);

%%%%%%%%%%%%%%%%%%%%%
%					%
%	Jack-knifing-1	%
%					%
%%%%%%%%%%%%%%%%%%%%%

[Ws_1, MUs_1, COVs_1, TLL_1] = train_audio2('./train_2/1', Ws_1, MUs_1, COVs_1, TLL_1 );
[Ws_2, MUs_2, COVs_2, TLL_2] = train_audio2('./train_2/2', Ws_2, MUs_2, COVs_2, TLL_2 );
[Ws_3, MUs_3, COVs_3, TLL_3] = train_audio2('./train_2/3', Ws_3, MUs_3, COVs_3, TLL_3 );
[Ws_4, MUs_4, COVs_4, TLL_4] = train_audio2('./train_2/4', Ws_4, MUs_4, COVs_4, TLL_4 );
[Ws_5, MUs_5, COVs_5, TLL_5] = train_audio2('./train_2/5', Ws_5, MUs_5, COVs_5, TLL_5 );
[Ws_6, MUs_6, COVs_6, TLL_6] = train_audio2('./train_2/6', Ws_6, MUs_6, COVs_6, TLL_6 );
[Ws_7, MUs_7, COVs_7, TLL_7] = train_audio2('./train_2/7', Ws_7, MUs_7, COVs_7, TLL_7 );
[Ws_8, MUs_8, COVs_8, TLL_8] = train_audio2('./train_2/8', Ws_8, MUs_8, COVs_8, TLL_8 );
[Ws_9, MUs_9, COVs_9, TLL_9] = train_audio2('./train_2/9', Ws_9, MUs_9, COVs_9, TLL_9 );
[Ws_10, MUs_10, COVs_10, TLL_10] = train_audio2('./train_2/10', Ws_10, MUs_10, COVs_10, TLL_10 );
[Ws_11, MUs_11, COVs_11, TLL_11] = train_audio2('./train_2/11', Ws_11, MUs_11, COVs_11, TLL_11 );
[Ws_12, MUs_12, COVs_12, TLL_12] = train_audio2('./train_2/12', Ws_12, MUs_12, COVs_12, TLL_12 );
[Ws_13, MUs_13, COVs_13, TLL_13] = train_audio2('./train_2/13', Ws_13, MUs_13, COVs_13, TLL_13 );
[Ws_14, MUs_14, COVs_14, TLL_14] = train_audio2('./train_2/14', Ws_14, MUs_14, COVs_14, TLL_14 );
[Ws_15, MUs_15, COVs_15, TLL_15] = train_audio2('./train_2/15', Ws_15, MUs_15, COVs_15, TLL_15 );
[Ws_16, MUs_16, COVs_16, TLL_16] = train_audio2('./train_2/16', Ws_16, MUs_16, COVs_16, TLL_16 );
[Ws_17, MUs_17, COVs_17, TLL_17] = train_audio2('./train_2/17', Ws_17, MUs_17, COVs_17, TLL_17 );
[Ws_18, MUs_18, COVs_18, TLL_18] = train_audio2('./train_2/18', Ws_18, MUs_18, COVs_18, TLL_18 );
[Ws_19, MUs_19, COVs_19, TLL_19] = train_audio2('./train_2/19', Ws_19, MUs_19, COVs_19, TLL_19 );
[Ws_20, MUs_20, COVs_20, TLL_20] = train_audio2('./train_2/20', Ws_20, MUs_20, COVs_20, TLL_20 );
[Ws_21, MUs_21, COVs_21, TLL_21] = train_audio2('./train_2/21', Ws_21, MUs_21, COVs_21, TLL_21 );
[Ws_22, MUs_22, COVs_22, TLL_22] = train_audio2('./train_2/22', Ws_22, MUs_22, COVs_22, TLL_22 );
[Ws_23, MUs_23, COVs_23, TLL_23] = train_audio2('./train_2/23', Ws_23, MUs_23, COVs_23, TLL_23 );
[Ws_24, MUs_24, COVs_24, TLL_24] = train_audio2('./train_2/24', Ws_24, MUs_24, COVs_24, TLL_24 );
[Ws_25, MUs_25, COVs_25, TLL_25] = train_audio2('./train_2/25', Ws_25, MUs_25, COVs_25, TLL_25 );
[Ws_26, MUs_26, COVs_26, TLL_26] = train_audio2('./train_2/26', Ws_26, MUs_26, COVs_26, TLL_26 );
[Ws_27, MUs_27, COVs_27, TLL_27] = train_audio2('./train_2/27', Ws_27, MUs_27, COVs_27, TLL_27 );
[Ws_28, MUs_28, COVs_28, TLL_28] = train_audio2('./train_2/28', Ws_28, MUs_28, COVs_28, TLL_28 );
[Ws_29, MUs_29, COVs_29, TLL_29] = train_audio2('./train_2/29', Ws_29, MUs_29, COVs_29, TLL_29 );
[Ws_30, MUs_30, COVs_30, TLL_30] = train_audio2('./train_2/30', Ws_30, MUs_30, COVs_30, TLL_30 );
[Ws_31, MUs_31, COVs_31, TLL_31] = train_audio2('./train_2/31', Ws_31, MUs_31, COVs_31, TLL_31 );

%%%%%%%%%%%%%%%%%%%%%
%					%
%	Jack-knifing-2	%
%					%
%%%%%%%%%%%%%%%%%%%%%

[Ws_1, MUs_1, COVs_1, TLL_1] = train_audio2('./all/1', Ws_1, MUs_1, COVs_1, TLL_1 );
[Ws_2, MUs_2, COVs_2, TLL_2] = train_audio2('./all/2', Ws_2, MUs_2, COVs_2, TLL_2 );
[Ws_3, MUs_3, COVs_3, TLL_3] = train_audio2('./all/3', Ws_3, MUs_3, COVs_3, TLL_3 );
[Ws_4, MUs_4, COVs_4, TLL_4] = train_audio2('./all/4', Ws_4, MUs_4, COVs_4, TLL_4 );
[Ws_5, MUs_5, COVs_5, TLL_5] = train_audio2('./all/5', Ws_5, MUs_5, COVs_5, TLL_5 );
[Ws_6, MUs_6, COVs_6, TLL_6] = train_audio2('./all/6', Ws_6, MUs_6, COVs_6, TLL_6 );
[Ws_7, MUs_7, COVs_7, TLL_7] = train_audio2('./all/7', Ws_7, MUs_7, COVs_7, TLL_7 );
[Ws_8, MUs_8, COVs_8, TLL_8] = train_audio2('./all/8', Ws_8, MUs_8, COVs_8, TLL_8 );
[Ws_9, MUs_9, COVs_9, TLL_9] = train_audio2('./all/9', Ws_9, MUs_9, COVs_9, TLL_9 );
[Ws_10, MUs_10, COVs_10, TLL_10] = train_audio2('./all/10', Ws_10, MUs_10, COVs_10, TLL_10 );
[Ws_11, MUs_11, COVs_11, TLL_11] = train_audio2('./all/11', Ws_11, MUs_11, COVs_11, TLL_11 );
[Ws_12, MUs_12, COVs_12, TLL_12] = train_audio2('./all/12', Ws_12, MUs_12, COVs_12, TLL_12 );
[Ws_13, MUs_13, COVs_13, TLL_13] = train_audio2('./all/13', Ws_13, MUs_13, COVs_13, TLL_13 );
[Ws_14, MUs_14, COVs_14, TLL_14] = train_audio2('./all/14', Ws_14, MUs_14, COVs_14, TLL_14 );
[Ws_15, MUs_15, COVs_15, TLL_15] = train_audio2('./all/15', Ws_15, MUs_15, COVs_15, TLL_15 );
[Ws_16, MUs_16, COVs_16, TLL_16] = train_audio2('./all/16', Ws_16, MUs_16, COVs_16, TLL_16 );
[Ws_17, MUs_17, COVs_17, TLL_17] = train_audio2('./all/17', Ws_17, MUs_17, COVs_17, TLL_17 );
[Ws_18, MUs_18, COVs_18, TLL_18] = train_audio2('./all/18', Ws_18, MUs_18, COVs_18, TLL_18 );
[Ws_19, MUs_19, COVs_19, TLL_19] = train_audio2('./all/19', Ws_19, MUs_19, COVs_19, TLL_19 );
[Ws_20, MUs_20, COVs_20, TLL_20] = train_audio2('./all/20', Ws_20, MUs_20, COVs_20, TLL_20 );
[Ws_21, MUs_21, COVs_21, TLL_21] = train_audio2('./all/21', Ws_21, MUs_21, COVs_21, TLL_21 );
[Ws_22, MUs_22, COVs_22, TLL_22] = train_audio2('./all/22', Ws_22, MUs_22, COVs_22, TLL_22 );
[Ws_23, MUs_23, COVs_23, TLL_23] = train_audio2('./all/23', Ws_23, MUs_23, COVs_23, TLL_23 );
[Ws_24, MUs_24, COVs_24, TLL_24] = train_audio2('./all/24', Ws_24, MUs_24, COVs_24, TLL_24 );
[Ws_25, MUs_25, COVs_25, TLL_25] = train_audio2('./all/25', Ws_25, MUs_25, COVs_25, TLL_25 );
[Ws_26, MUs_26, COVs_26, TLL_26] = train_audio2('./all/26', Ws_26, MUs_26, COVs_26, TLL_26 );
[Ws_27, MUs_27, COVs_27, TLL_27] = train_audio2('./all/27', Ws_27, MUs_27, COVs_27, TLL_27 );
[Ws_28, MUs_28, COVs_28, TLL_28] = train_audio2('./all/28', Ws_28, MUs_28, COVs_28, TLL_28 );
[Ws_29, MUs_29, COVs_29, TLL_29] = train_audio2('./all/29', Ws_29, MUs_29, COVs_29, TLL_29 );
[Ws_30, MUs_30, COVs_30, TLL_30] = train_audio2('./all/30', Ws_30, MUs_30, COVs_30, TLL_30 );
[Ws_31, MUs_31, COVs_31, TLL_31] = train_audio2('./all/31', Ws_31, MUs_31, COVs_31, TLL_31 );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%							%
%	Test na dalsich datach	%
%							%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Nacitame si data, na ktorych budeme testovat
[test_1 files_1] = wav16khz2mfcc('./dev_all');
test_1 = process_data(test_1);

% Apriorna pravdepodobnost tried
P = 1/31;

result_file = fopen('audio_gmm_result.txt','w');

test_set=test_1;
file_names = files_1;

for ii=1:length(test_set)
  [dir, file_name, ext] = fileparts(file_names{ii});
  ll(1) = sum( logpdf_gmm(test_set{ii}, Ws_1, MUs_1, COVs_1) );
  ll(2) = sum( logpdf_gmm(test_set{ii}, Ws_2, MUs_2, COVs_2) );
  ll(3) = sum( logpdf_gmm(test_set{ii}, Ws_3, MUs_3, COVs_3) );
  ll(4) = sum( logpdf_gmm(test_set{ii}, Ws_4, MUs_4, COVs_4) );
  ll(5) = sum( logpdf_gmm(test_set{ii}, Ws_5, MUs_5, COVs_5) );
  ll(6) = sum( logpdf_gmm(test_set{ii}, Ws_6, MUs_6, COVs_6) );
  ll(7) = sum( logpdf_gmm(test_set{ii}, Ws_7, MUs_7, COVs_7) );
  ll(8) = sum( logpdf_gmm(test_set{ii}, Ws_8, MUs_8, COVs_8) );
  ll(9) = sum( logpdf_gmm(test_set{ii}, Ws_9, MUs_9, COVs_9) );
  ll(10) = sum( logpdf_gmm(test_set{ii}, Ws_10, MUs_10, COVs_10) );
  ll(11) = sum( logpdf_gmm(test_set{ii}, Ws_11, MUs_11, COVs_11) );
  ll(12) = sum( logpdf_gmm(test_set{ii}, Ws_12, MUs_12, COVs_12) );
  ll(13) = sum( logpdf_gmm(test_set{ii}, Ws_13, MUs_13, COVs_13) );
  ll(14) = sum( logpdf_gmm(test_set{ii}, Ws_14, MUs_14, COVs_14) );
  ll(15) = sum( logpdf_gmm(test_set{ii}, Ws_15, MUs_15, COVs_15) );
  ll(16) = sum( logpdf_gmm(test_set{ii}, Ws_16, MUs_16, COVs_16) );
  ll(17) = sum( logpdf_gmm(test_set{ii}, Ws_17, MUs_17, COVs_17) );
  ll(18) = sum( logpdf_gmm(test_set{ii}, Ws_18, MUs_18, COVs_18) );
  ll(19) = sum( logpdf_gmm(test_set{ii}, Ws_19, MUs_19, COVs_19) );
  ll(20) = sum( logpdf_gmm(test_set{ii}, Ws_20, MUs_20, COVs_20) );
  ll(21) = sum( logpdf_gmm(test_set{ii}, Ws_21, MUs_21, COVs_21) );
  ll(22) = sum( logpdf_gmm(test_set{ii}, Ws_22, MUs_22, COVs_22) );
  ll(23) = sum( logpdf_gmm(test_set{ii}, Ws_23, MUs_23, COVs_23) );
  ll(24) = sum( logpdf_gmm(test_set{ii}, Ws_24, MUs_24, COVs_24) );
  ll(25) = sum( logpdf_gmm(test_set{ii}, Ws_25, MUs_25, COVs_25) );
  ll(26) = sum( logpdf_gmm(test_set{ii}, Ws_26, MUs_26, COVs_26) );
  ll(27) = sum( logpdf_gmm(test_set{ii}, Ws_27, MUs_27, COVs_27) );
  ll(28) = sum( logpdf_gmm(test_set{ii}, Ws_28, MUs_28, COVs_28) );
  ll(29) = sum( logpdf_gmm(test_set{ii}, Ws_29, MUs_29, COVs_29) );
  ll(30) = sum( logpdf_gmm(test_set{ii}, Ws_30, MUs_30, COVs_30) );
  ll(31) = sum( logpdf_gmm(test_set{ii}, Ws_31, MUs_31, COVs_31) );
  score{ii} = ll;
  [m trieda] = max(ll);
  fprintf(result_file, '%s %d %010.2f ', file_name, trieda, ll);
  fprintf(result_file, '\r\n');
  %disp([file_name '\t' num2str(trieda) '\t' num2str(ll, '12.5%f\t')])%do suboru!! aktualne skore je score{ii} pre vsetky tried
end

fclose(result_file);

cd ./SRC
