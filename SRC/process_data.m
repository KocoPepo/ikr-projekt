function data = process_data(data)

for ii = 1:length(data)
	data{ii} = remove1sec(data{ii});
	data{ii} = remove_silence(data{ii});
	data{ii} = normalize(data{ii});
end