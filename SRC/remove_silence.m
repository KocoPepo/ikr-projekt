% vymaze nizke hodnoty z nahravky (ticho)
function X = remove_silence(X)
% limit sluzi ako horna hranica ticha, vsetko nad limitov uz ticho nie je
limit = min(X(1,:)) + 3;
X(:,(X(1,:) < limit)) = [];